module Discente
    class Aluno
        attr_accessor :nome, :matricula, :periodo, :curso 
    end
    
    class AlunoGraduacao < Aluno
        def initialize(cr, carga)
            @cr = cr
            @carga = carga
        end

        def cr
            @cr
        end

        def carga
            @carga
        end

        def salvar_class
            {cr: @cr, carga: @carga, nome: @nome, matricula: @matricula, curso:@curso}
        end
    end

    class AlunoMestrado < Aluno

        def initialize(orientador,graduacao, area_pesquisa)
        @graduacao,= graduacao
        @orientador = orientador
        @area_pesquisa = area_pesquisa
        end
        
        def salvar_class
            {nome:@nome, matricula:@matricula, graduacao:@graduacao, orientador:@orientador, area_pesquisa:@area_pesquisa}
        end
    end
end   
